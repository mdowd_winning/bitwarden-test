const util = require('util');
const exec = util.promisify(require('child_process').exec);
const { spawn } = require('child_process');
const fs = require('fs').promises;

/*
    Helper Functions
 */

async function runIoCmd(cmd) {
    return new Promise((resolve, reject) => {
        const childProc = spawn(cmd, {
            stdio: ['inherit', 'pipe', 'inherit'],
            shell: true,
            cwd: process.cwd()
        });
    
        let result;
    
        childProc.on('error', reject);
        childProc.stdout.on('data', data => result = data.toString());
        childProc.stdout.on('end', () => {
            resolve(result);
        });
    });
}

async function writeJsonFile(path, obj) {
    await fs.writeFile(path, JSON.stringify(obj, null, 2), { encoding: 'utf-8', flag: 'w' });
}

/*
    Bitwarden Commands
 */

async function getStatus() {
    const { stdout } = await exec('npx bw status');
    return JSON.parse(stdout);
}

async function unlock() {
    console.log('Please enter your Bitwarden password to retrieve app secrets.');

    return await runIoCmd('npx bw unlock --raw');
}

async function login() {
    console.log('Please log into Bitwarden to retrieve app secrets.');

    return await runIoCmd('npx bw login --raw');
}

async function syncItems(sessionKey) {
    console.log('Syncing Bitwarden secrets from the server...')

    await exec('npx bw sync --session=' + sessionKey);
}

async function getSecrets(itemName, sessionKey) {
    console.log('Parsing fields from ' + itemName);

    const { stdout } = await exec('npx bw get item ' + itemName + ' --session=' + sessionKey);
    const result = JSON.parse(stdout);

    const settings = {};

    for (const field of result.fields) {
        const parts = field.name.split('.');
        let scope = settings;
        
        for (let i = 0; i < parts.length - 1; i++) {
          scope = scope[parts[i]] = {};
        }
        
        scope[parts[parts.length - 1]] = field.value;
    }

    return settings;
}

/*
    Main
 */

async function main() {
    let sessionKey;

    const args = process.argv.slice(2);

    if (args.length < 1) {
        console.error('Need to specify a secret name to retrieve.');
        return;
    }

    const secretName = args[0];
    const fileName = args.length > 1 ? args[1] : 'appsettings.json';

    const status = await getStatus();

    if (status.status === 'unauthenticated') {
        sessionKey = await login();
    } else if (status.status === 'locked') {
        sessionKey = await unlock();
    }

    await syncItems(sessionKey);
    const settings = await getSecrets(secretName, sessionKey);

    console.log('Writing to "' + fileName + '"');
    await writeJsonFile(fileName, settings);
}

main()
    .then(() => console.log('Done.'))
    .catch(reason => console.log(reason));
