# Messing with Bitwarden

Wanting to use Bitwarden as a secrets repository. Let's see if this works...

## Usage:

1. Run `npm install`
2. Run `npm run get-secrets`
3. Log in with your Bitwarden credentials
4. Notice that `appsecrets.json` was created.
